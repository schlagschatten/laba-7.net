﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using ThreadState = System.Threading.ThreadState;

namespace ConsoleApp3
{
    class Program
    {
        static double SizeOfFolder(string folder)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(folder);
                Console.WriteLine($"Thread {Thread.CurrentThread.Name} work with {di.FullName}");
                DirectoryInfo[] diA = di.GetDirectories();
                FileInfo[] fi = di.GetFiles();

                double catalogSize = fi.Aggregate<FileInfo, double>(0, (current, f) => current + f.Length);

                Mutex m1 = new Mutex();
                Thread[] threads = diA.Select<DirectoryInfo, Thread>((dir) =>
                {
                    return new Thread(() =>
                    {
                        m1.WaitOne();
                        catalogSize += SizeOfFolder(dir.FullName);
                        m1.ReleaseMutex();
                    })
                    {
                        Name = dir.Name 
                    };
                }).ToArray();

                foreach (var t in threads)
                {
                    t.Start();
                }

                while (!((threads.All(t => t.ThreadState == ThreadState.Stopped))))
                {
                    Thread.Sleep(5);
                }
                
                Console.WriteLine(folder);
                Console.WriteLine(catalogSize / 1024 + " Кб");
                Console.WriteLine("");
                return catalogSize;
            }

            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine("no such directory. ERROR: " + ex.Message);
                return 0;
            }

            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("not access. ERROR: " + ex.Message);
                return 0;
            }

            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                return 0;
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Input path");
            string path = Console.ReadLine();

            if (Directory.Exists(path))
            {
                Console.WriteLine(SizeOfFolder(path) / 1024 + " Кб");
            }
            else
            {
                Console.WriteLine("Nothing");
            }
        }
    }
}